# Clipgrab

## Description

ClipGrab is a free downloader and converter for videos from Youtube, Vimeo, Dailymotion and many other video sites.

## What is ClipGrab?

<img align="justify" alt="Sreenshot_VideoSearch" src="https://clipgrab.de/img/screenshot_videosearch_x11.png" />

ClipGrab is free software for downloading and converting videos from sites such as YouTube or Vimeo.
ClipGrab can download from the following sites: Clipfish, Collegehumor, Dailymotion, MyVideo, MySpass, Sevenload, Tudou, Vimeo. 
Downloaded videos can be converted to the following formats: WMV, MPEG4, OGG Theora, MP3 (audio only), OGG Vorbis (audio only).

## What else can ClipGrab do?

<img align="justify" alt="Screenshot_Download" src="https://clipgrab.de/img/screenshot_download_x11.png" />

ClipGrab is not limited to the aforementioned sites, because other sites are "unofficially" supported through ClipGrab's official site recognition - Try it!

## Heads up

<img align="left" alt="Heads up" src="https://gitlab.com/amaroklinux/images/-/raw/main/icons/aviso.png" width="40"/>

Amarok Linux is **NOT** responsible for any illegal acts performed through this application. The responsibility lies entirely with the user who is using it for illicit purposes or of any other nature!

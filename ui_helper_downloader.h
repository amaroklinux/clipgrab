/********************************************************************************
** Form generated from reading UI file 'helper_downloader.ui'
**
** Created by: Qt User Interface Compiler version 5.15.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HELPER_DOWNLOADER_H
#define UI_HELPER_DOWNLOADER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_HelperDownloader
{
public:
    QGridLayout *gridLayout;
    QPushButton *continueButton;
    QPushButton *exitButton;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QProgressBar *progressBar;
    QWidget *widget;
    QGridLayout *gridLayout_2;
    QLabel *labelInfoText;
    QLabel *label;
    QWidget *widget_2;
    QFormLayout *formLayout;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer;

    void setupUi(QDialog *HelperDownloader)
    {
        if (HelperDownloader->objectName().isEmpty())
            HelperDownloader->setObjectName(QString::fromUtf8("HelperDownloader"));
        HelperDownloader->resize(498, 300);
        gridLayout = new QGridLayout(HelperDownloader);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        continueButton = new QPushButton(HelperDownloader);
        continueButton->setObjectName(QString::fromUtf8("continueButton"));

        gridLayout->addWidget(continueButton, 3, 2, 1, 1);

        exitButton = new QPushButton(HelperDownloader);
        exitButton->setObjectName(QString::fromUtf8("exitButton"));

        gridLayout->addWidget(exitButton, 3, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 75, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 1, 2, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 3, 0, 1, 1);

        progressBar = new QProgressBar(HelperDownloader);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);

        gridLayout->addWidget(progressBar, 2, 0, 1, 3);

        widget = new QWidget(HelperDownloader);
        widget->setObjectName(QString::fromUtf8("widget"));
        gridLayout_2 = new QGridLayout(widget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        labelInfoText = new QLabel(widget);
        labelInfoText->setObjectName(QString::fromUtf8("labelInfoText"));
        labelInfoText->setWordWrap(true);

        gridLayout_2->addWidget(labelInfoText, 1, 2, 1, 1);

        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("color:#00b2de;\n"
"font-size: 18px;"));

        gridLayout_2->addWidget(label, 0, 2, 1, 1);

        widget_2 = new QWidget(widget);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget_2->sizePolicy().hasHeightForWidth());
        widget_2->setSizePolicy(sizePolicy);
        widget_2->setMinimumSize(QSize(64, 64));
        formLayout = new QFormLayout(widget_2);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setHorizontalSpacing(0);
        formLayout->setVerticalSpacing(0);
        formLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(widget_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(64, 64));
        label_2->setMaximumSize(QSize(32, 32));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/img/icon.png")));
        label_2->setScaledContents(true);

        formLayout->setWidget(0, QFormLayout::LabelRole, label_2);


        gridLayout_2->addWidget(widget_2, 0, 0, 2, 1);

        horizontalSpacer = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 0, 1, 1, 1);


        gridLayout->addWidget(widget, 0, 0, 1, 3);


        retranslateUi(HelperDownloader);

        QMetaObject::connectSlotsByName(HelperDownloader);
    } // setupUi

    void retranslateUi(QDialog *HelperDownloader)
    {
        HelperDownloader->setWindowTitle(QCoreApplication::translate("HelperDownloader", "Dialog", nullptr));
        continueButton->setText(QCoreApplication::translate("HelperDownloader", "Continue", nullptr));
        exitButton->setText(QCoreApplication::translate("HelperDownloader", "Exit ClipGrab", nullptr));
        labelInfoText->setText(QCoreApplication::translate("HelperDownloader", "<html><head/><body><p>ClipGrab uses youtube-dlp in order to download videos from the Internet. youtube-dlp is developed by an independent team of Open Source developers and released into the public domain.<br/>Learn more on <a href=\"https://github.com/yt-dlp/yt-dlp\"><span style=\" text-decoration: underline; color:#0068da;\">github.com/yt-dlp/yt-dlp</span></a>.</p><p>Click on <span style=\" font-style:italic;\">Continue</span> to download youtube-dlp.</p></body></html>", nullptr));
        label->setText(QCoreApplication::translate("HelperDownloader", "Downloading youtube-dlp", nullptr));
    } // retranslateUi

};

namespace Ui {
    class HelperDownloader: public Ui_HelperDownloader {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HELPER_DOWNLOADER_H

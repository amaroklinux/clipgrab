/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.8)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.8. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SearchWebEngineUrlRequestInterceptor_t {
    QByteArrayData data[4];
    char stringdata0[54];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SearchWebEngineUrlRequestInterceptor_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SearchWebEngineUrlRequestInterceptor_t qt_meta_stringdata_SearchWebEngineUrlRequestInterceptor = {
    {
QT_MOC_LITERAL(0, 0, 36), // "SearchWebEngineUrlRequestInte..."
QT_MOC_LITERAL(1, 37, 11), // "intercepted"
QT_MOC_LITERAL(2, 49, 0), // ""
QT_MOC_LITERAL(3, 50, 3) // "url"

    },
    "SearchWebEngineUrlRequestInterceptor\0"
    "intercepted\0\0url"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SearchWebEngineUrlRequestInterceptor[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QUrl,    3,

       0        // eod
};

void SearchWebEngineUrlRequestInterceptor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SearchWebEngineUrlRequestInterceptor *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->intercepted((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (SearchWebEngineUrlRequestInterceptor::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SearchWebEngineUrlRequestInterceptor::intercepted)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SearchWebEngineUrlRequestInterceptor::staticMetaObject = { {
    QMetaObject::SuperData::link<QWebEngineUrlRequestInterceptor::staticMetaObject>(),
    qt_meta_stringdata_SearchWebEngineUrlRequestInterceptor.data,
    qt_meta_data_SearchWebEngineUrlRequestInterceptor,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SearchWebEngineUrlRequestInterceptor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SearchWebEngineUrlRequestInterceptor::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SearchWebEngineUrlRequestInterceptor.stringdata0))
        return static_cast<void*>(this);
    return QWebEngineUrlRequestInterceptor::qt_metacast(_clname);
}

int SearchWebEngineUrlRequestInterceptor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWebEngineUrlRequestInterceptor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void SearchWebEngineUrlRequestInterceptor::intercepted(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
struct qt_meta_stringdata_SearchWebEnginePage_t {
    QByteArrayData data[6];
    char stringdata0[74];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SearchWebEnginePage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SearchWebEnginePage_t qt_meta_stringdata_SearchWebEnginePage = {
    {
QT_MOC_LITERAL(0, 0, 19), // "SearchWebEnginePage"
QT_MOC_LITERAL(1, 20, 11), // "linkClicked"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 3), // "url"
QT_MOC_LITERAL(4, 37, 15), // "linkIntercepted"
QT_MOC_LITERAL(5, 53, 20) // "handleInterceptedUrl"

    },
    "SearchWebEnginePage\0linkClicked\0\0url\0"
    "linkIntercepted\0handleInterceptedUrl"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SearchWebEnginePage[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x06 /* Public */,
       4,    1,   32,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   35,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QUrl,    3,
    QMetaType::Void, QMetaType::QUrl,    3,

 // slots: parameters
    QMetaType::Void, QMetaType::QUrl,    3,

       0        // eod
};

void SearchWebEnginePage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SearchWebEnginePage *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->linkClicked((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 1: _t->linkIntercepted((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 2: _t->handleInterceptedUrl((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (SearchWebEnginePage::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SearchWebEnginePage::linkClicked)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (SearchWebEnginePage::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SearchWebEnginePage::linkIntercepted)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SearchWebEnginePage::staticMetaObject = { {
    QMetaObject::SuperData::link<QWebEnginePage::staticMetaObject>(),
    qt_meta_stringdata_SearchWebEnginePage.data,
    qt_meta_data_SearchWebEnginePage,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SearchWebEnginePage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SearchWebEnginePage::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SearchWebEnginePage.stringdata0))
        return static_cast<void*>(this);
    return QWebEnginePage::qt_metacast(_clname);
}

int SearchWebEnginePage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWebEnginePage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void SearchWebEnginePage::linkClicked(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void SearchWebEnginePage::linkIntercepted(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[53];
    char stringdata0[1313];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 13), // "startDownload"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 29), // "compatibleUrlFoundInClipBoard"
QT_MOC_LITERAL(4, 56, 3), // "url"
QT_MOC_LITERAL(5, 60, 18), // "targetFileSelected"
QT_MOC_LITERAL(6, 79, 6), // "video*"
QT_MOC_LITERAL(7, 86, 5), // "video"
QT_MOC_LITERAL(8, 92, 6), // "target"
QT_MOC_LITERAL(9, 99, 18), // "searchTimerTimeout"
QT_MOC_LITERAL(10, 118, 30), // "handleCurrentVideoStateChanged"
QT_MOC_LITERAL(11, 149, 25), // "on_mainTab_currentChanged"
QT_MOC_LITERAL(12, 175, 5), // "index"
QT_MOC_LITERAL(13, 181, 42), // "on_downloadComboFormat_curren..."
QT_MOC_LITERAL(14, 224, 29), // "on_searchLineEdit_textChanged"
QT_MOC_LITERAL(15, 254, 35), // "on_settingsUseMetadata_stateC..."
QT_MOC_LITERAL(16, 290, 22), // "on_label_linkActivated"
QT_MOC_LITERAL(17, 313, 4), // "link"
QT_MOC_LITERAL(18, 318, 33), // "on_downloadLineEdit_returnPre..."
QT_MOC_LITERAL(19, 352, 38), // "on_settingsMinimizeToTray_sta..."
QT_MOC_LITERAL(20, 391, 24), // "on_downloadPause_clicked"
QT_MOC_LITERAL(21, 416, 47), // "on_settingsRemoveFinishedDown..."
QT_MOC_LITERAL(22, 464, 34), // "handle_downloadTree_currentCh..."
QT_MOC_LITERAL(23, 499, 11), // "QModelIndex"
QT_MOC_LITERAL(24, 511, 7), // "current"
QT_MOC_LITERAL(25, 519, 8), // "previous"
QT_MOC_LITERAL(26, 528, 24), // "systemTrayMessageClicked"
QT_MOC_LITERAL(27, 553, 23), // "systemTrayIconActivated"
QT_MOC_LITERAL(28, 577, 33), // "QSystemTrayIcon::ActivationRe..."
QT_MOC_LITERAL(29, 611, 23), // "on_downloadOpen_clicked"
QT_MOC_LITERAL(30, 635, 36), // "on_settingsSaveLastPath_state..."
QT_MOC_LITERAL(31, 672, 25), // "on_downloadCancel_clicked"
QT_MOC_LITERAL(32, 698, 35), // "on_settingsBrowseTargetPath_c..."
QT_MOC_LITERAL(33, 734, 32), // "on_settingsSavedPath_textChanged"
QT_MOC_LITERAL(34, 767, 39), // "on_settingsNeverAskForPath_st..."
QT_MOC_LITERAL(35, 807, 25), // "settingsClipboard_toggled"
QT_MOC_LITERAL(36, 833, 29), // "settingsNotifications_toggled"
QT_MOC_LITERAL(37, 863, 20), // "settingsProxyChanged"
QT_MOC_LITERAL(38, 884, 19), // "handleSearchResults"
QT_MOC_LITERAL(39, 904, 25), // "handleSearchResultClicked"
QT_MOC_LITERAL(40, 930, 24), // "handleFinishedConversion"
QT_MOC_LITERAL(41, 955, 39), // "on_settingsLanguage_currentIn..."
QT_MOC_LITERAL(42, 995, 23), // "on_buttonDonate_clicked"
QT_MOC_LITERAL(43, 1019, 26), // "on_settingsUseWebM_toggled"
QT_MOC_LITERAL(44, 1046, 7), // "checked"
QT_MOC_LITERAL(45, 1054, 34), // "on_settingsIgnoreSSLErrors_to..."
QT_MOC_LITERAL(46, 1089, 42), // "on_downloadTree_customContext..."
QT_MOC_LITERAL(47, 1132, 3), // "pos"
QT_MOC_LITERAL(48, 1136, 33), // "on_settingsRememberLogins_tog..."
QT_MOC_LITERAL(49, 1170, 39), // "on_settingsRememberVideoQuali..."
QT_MOC_LITERAL(50, 1210, 43), // "on_downloadComboQuality_curre..."
QT_MOC_LITERAL(51, 1254, 29), // "on_downloadTree_doubleClicked"
QT_MOC_LITERAL(52, 1284, 28) // "on_settingsForceIpV4_toggled"

    },
    "MainWindow\0startDownload\0\0"
    "compatibleUrlFoundInClipBoard\0url\0"
    "targetFileSelected\0video*\0video\0target\0"
    "searchTimerTimeout\0handleCurrentVideoStateChanged\0"
    "on_mainTab_currentChanged\0index\0"
    "on_downloadComboFormat_currentIndexChanged\0"
    "on_searchLineEdit_textChanged\0"
    "on_settingsUseMetadata_stateChanged\0"
    "on_label_linkActivated\0link\0"
    "on_downloadLineEdit_returnPressed\0"
    "on_settingsMinimizeToTray_stateChanged\0"
    "on_downloadPause_clicked\0"
    "on_settingsRemoveFinishedDownloads_stateChanged\0"
    "handle_downloadTree_currentChanged\0"
    "QModelIndex\0current\0previous\0"
    "systemTrayMessageClicked\0"
    "systemTrayIconActivated\0"
    "QSystemTrayIcon::ActivationReason\0"
    "on_downloadOpen_clicked\0"
    "on_settingsSaveLastPath_stateChanged\0"
    "on_downloadCancel_clicked\0"
    "on_settingsBrowseTargetPath_clicked\0"
    "on_settingsSavedPath_textChanged\0"
    "on_settingsNeverAskForPath_stateChanged\0"
    "settingsClipboard_toggled\0"
    "settingsNotifications_toggled\0"
    "settingsProxyChanged\0handleSearchResults\0"
    "handleSearchResultClicked\0"
    "handleFinishedConversion\0"
    "on_settingsLanguage_currentIndexChanged\0"
    "on_buttonDonate_clicked\0"
    "on_settingsUseWebM_toggled\0checked\0"
    "on_settingsIgnoreSSLErrors_toggled\0"
    "on_downloadTree_customContextMenuRequested\0"
    "pos\0on_settingsRememberLogins_toggled\0"
    "on_settingsRememberVideoQuality_toggled\0"
    "on_downloadComboQuality_currentIndexChanged\0"
    "on_downloadTree_doubleClicked\0"
    "on_settingsForceIpV4_toggled"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      39,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  209,    2, 0x0a /* Public */,
       3,    1,  210,    2, 0x0a /* Public */,
       5,    2,  213,    2, 0x0a /* Public */,
       9,    0,  218,    2, 0x0a /* Public */,
      10,    1,  219,    2, 0x08 /* Private */,
      11,    1,  222,    2, 0x08 /* Private */,
      13,    1,  225,    2, 0x08 /* Private */,
      14,    1,  228,    2, 0x08 /* Private */,
      15,    1,  231,    2, 0x08 /* Private */,
      16,    1,  234,    2, 0x08 /* Private */,
      18,    0,  237,    2, 0x08 /* Private */,
      19,    1,  238,    2, 0x08 /* Private */,
      20,    0,  241,    2, 0x08 /* Private */,
      21,    1,  242,    2, 0x08 /* Private */,
      22,    2,  245,    2, 0x08 /* Private */,
      26,    0,  250,    2, 0x08 /* Private */,
      27,    1,  251,    2, 0x08 /* Private */,
      29,    0,  254,    2, 0x08 /* Private */,
      30,    1,  255,    2, 0x08 /* Private */,
      31,    0,  258,    2, 0x08 /* Private */,
      32,    0,  259,    2, 0x08 /* Private */,
      33,    1,  260,    2, 0x08 /* Private */,
      34,    1,  263,    2, 0x08 /* Private */,
      35,    1,  266,    2, 0x08 /* Private */,
      36,    1,  269,    2, 0x08 /* Private */,
      37,    0,  272,    2, 0x08 /* Private */,
      38,    1,  273,    2, 0x08 /* Private */,
      39,    1,  276,    2, 0x08 /* Private */,
      40,    1,  279,    2, 0x08 /* Private */,
      41,    1,  282,    2, 0x08 /* Private */,
      42,    0,  285,    2, 0x08 /* Private */,
      43,    1,  286,    2, 0x08 /* Private */,
      45,    1,  289,    2, 0x08 /* Private */,
      46,    1,  292,    2, 0x08 /* Private */,
      48,    1,  295,    2, 0x08 /* Private */,
      49,    1,  298,    2, 0x08 /* Private */,
      50,    1,  301,    2, 0x08 /* Private */,
      51,    1,  304,    2, 0x08 /* Private */,
      52,    1,  307,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void, 0x80000000 | 6, QMetaType::QString,    7,    8,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    2,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::QString,   17,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, 0x80000000 | 23, 0x80000000 | 23,   24,   25,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 28,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    2,
    QMetaType::Void, QMetaType::QUrl,    4,
    QMetaType::Void, 0x80000000 | 6,    2,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   44,
    QMetaType::Void, QMetaType::Bool,   44,
    QMetaType::Void, QMetaType::QPoint,   47,
    QMetaType::Void, QMetaType::Bool,   44,
    QMetaType::Void, QMetaType::Bool,   44,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, 0x80000000 | 23,   12,
    QMetaType::Void, QMetaType::Bool,   44,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->startDownload(); break;
        case 1: _t->compatibleUrlFoundInClipBoard((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->targetFileSelected((*reinterpret_cast< video*(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->searchTimerTimeout(); break;
        case 4: _t->handleCurrentVideoStateChanged((*reinterpret_cast< video*(*)>(_a[1]))); break;
        case 5: _t->on_mainTab_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_downloadComboFormat_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_searchLineEdit_textChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->on_settingsUseMetadata_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_label_linkActivated((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->on_downloadLineEdit_returnPressed(); break;
        case 11: _t->on_settingsMinimizeToTray_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_downloadPause_clicked(); break;
        case 13: _t->on_settingsRemoveFinishedDownloads_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->handle_downloadTree_currentChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 15: _t->systemTrayMessageClicked(); break;
        case 16: _t->systemTrayIconActivated((*reinterpret_cast< QSystemTrayIcon::ActivationReason(*)>(_a[1]))); break;
        case 17: _t->on_downloadOpen_clicked(); break;
        case 18: _t->on_settingsSaveLastPath_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->on_downloadCancel_clicked(); break;
        case 20: _t->on_settingsBrowseTargetPath_clicked(); break;
        case 21: _t->on_settingsSavedPath_textChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 22: _t->on_settingsNeverAskForPath_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->settingsClipboard_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 24: _t->settingsNotifications_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: _t->settingsProxyChanged(); break;
        case 26: _t->handleSearchResults((*reinterpret_cast< video*(*)>(_a[1]))); break;
        case 27: _t->handleSearchResultClicked((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 28: _t->handleFinishedConversion((*reinterpret_cast< video*(*)>(_a[1]))); break;
        case 29: _t->on_settingsLanguage_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 30: _t->on_buttonDonate_clicked(); break;
        case 31: _t->on_settingsUseWebM_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 32: _t->on_settingsIgnoreSSLErrors_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 33: _t->on_downloadTree_customContextMenuRequested((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 34: _t->on_settingsRememberLogins_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 35: _t->on_settingsRememberVideoQuality_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 36: _t->on_downloadComboQuality_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 37: _t->on_downloadTree_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 38: _t->on_settingsForceIpV4_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< video* >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< video* >(); break;
            }
            break;
        case 26:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< video* >(); break;
            }
            break;
        case 28:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< video* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 39)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 39;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 39)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 39;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

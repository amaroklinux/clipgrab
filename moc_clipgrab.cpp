/****************************************************************************
** Meta object code from reading C++ file 'clipgrab.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.8)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "clipgrab.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'clipgrab.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.8. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ClipGrab_t {
    QByteArrayData data[30];
    char stringdata0[464];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ClipGrab_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ClipGrab_t qt_meta_stringdata_ClipGrab = {
    {
QT_MOC_LITERAL(0, 0, 8), // "ClipGrab"
QT_MOC_LITERAL(1, 9, 24), // "currentVideoStateChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 6), // "video*"
QT_MOC_LITERAL(4, 42, 16), // "downloadEnqueued"
QT_MOC_LITERAL(5, 59, 16), // "downloadFinished"
QT_MOC_LITERAL(6, 76, 14), // "searchFinished"
QT_MOC_LITERAL(7, 91, 25), // "youtubeDlDownloadFinished"
QT_MOC_LITERAL(8, 117, 29), // "compatibleUrlFoundInClipboard"
QT_MOC_LITERAL(9, 147, 3), // "url"
QT_MOC_LITERAL(10, 151, 20), // "allDownloadsCanceled"
QT_MOC_LITERAL(11, 172, 19), // "updateInfoProcessed"
QT_MOC_LITERAL(12, 192, 12), // "errorHandler"
QT_MOC_LITERAL(13, 205, 15), // "parseUpdateInfo"
QT_MOC_LITERAL(14, 221, 14), // "QNetworkReply*"
QT_MOC_LITERAL(15, 236, 5), // "reply"
QT_MOC_LITERAL(16, 242, 14), // "fetchVideoInfo"
QT_MOC_LITERAL(17, 257, 17), // "clearCurrentVideo"
QT_MOC_LITERAL(18, 275, 15), // "enqueueDownload"
QT_MOC_LITERAL(19, 291, 5), // "video"
QT_MOC_LITERAL(20, 297, 18), // "cancelAllDownloads"
QT_MOC_LITERAL(21, 316, 6), // "search"
QT_MOC_LITERAL(22, 323, 8), // "keywords"
QT_MOC_LITERAL(23, 332, 16), // "clipboardChanged"
QT_MOC_LITERAL(24, 349, 21), // "activateProxySettings"
QT_MOC_LITERAL(25, 371, 19), // "startUpdateDownload"
QT_MOC_LITERAL(26, 391, 10), // "skipUpdate"
QT_MOC_LITERAL(27, 402, 22), // "updateDownloadFinished"
QT_MOC_LITERAL(28, 425, 22), // "updateDownloadProgress"
QT_MOC_LITERAL(29, 448, 15) // "updateReadyRead"

    },
    "ClipGrab\0currentVideoStateChanged\0\0"
    "video*\0downloadEnqueued\0downloadFinished\0"
    "searchFinished\0youtubeDlDownloadFinished\0"
    "compatibleUrlFoundInClipboard\0url\0"
    "allDownloadsCanceled\0updateInfoProcessed\0"
    "errorHandler\0parseUpdateInfo\0"
    "QNetworkReply*\0reply\0fetchVideoInfo\0"
    "clearCurrentVideo\0enqueueDownload\0"
    "video\0cancelAllDownloads\0search\0"
    "keywords\0clipboardChanged\0"
    "activateProxySettings\0startUpdateDownload\0"
    "skipUpdate\0updateDownloadFinished\0"
    "updateDownloadProgress\0updateReadyRead"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ClipGrab[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  134,    2, 0x06 /* Public */,
       4,    0,  137,    2, 0x06 /* Public */,
       5,    1,  138,    2, 0x06 /* Public */,
       6,    1,  141,    2, 0x06 /* Public */,
       7,    0,  144,    2, 0x06 /* Public */,
       8,    1,  145,    2, 0x06 /* Public */,
      10,    0,  148,    2, 0x06 /* Public */,
      11,    0,  149,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    1,  150,    2, 0x0a /* Public */,
      12,    2,  153,    2, 0x0a /* Public */,
      13,    1,  158,    2, 0x0a /* Public */,
      16,    1,  161,    2, 0x0a /* Public */,
      17,    0,  164,    2, 0x0a /* Public */,
      18,    1,  165,    2, 0x0a /* Public */,
      20,    0,  168,    2, 0x0a /* Public */,
      21,    1,  169,    2, 0x0a /* Public */,
      21,    0,  172,    2, 0x2a /* Public | MethodCloned */,
      23,    0,  173,    2, 0x0a /* Public */,
      24,    0,  174,    2, 0x0a /* Public */,
      25,    0,  175,    2, 0x0a /* Public */,
      26,    0,  176,    2, 0x0a /* Public */,
      27,    0,  177,    2, 0x0a /* Public */,
      28,    2,  178,    2, 0x0a /* Public */,
      29,    0,  183,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 3,    2,    2,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,   19,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   22,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::LongLong, QMetaType::LongLong,    2,    2,
    QMetaType::Void,

       0        // eod
};

void ClipGrab::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ClipGrab *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->currentVideoStateChanged((*reinterpret_cast< video*(*)>(_a[1]))); break;
        case 1: _t->downloadEnqueued(); break;
        case 2: _t->downloadFinished((*reinterpret_cast< video*(*)>(_a[1]))); break;
        case 3: _t->searchFinished((*reinterpret_cast< video*(*)>(_a[1]))); break;
        case 4: _t->youtubeDlDownloadFinished(); break;
        case 5: _t->compatibleUrlFoundInClipboard((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->allDownloadsCanceled(); break;
        case 7: _t->updateInfoProcessed(); break;
        case 8: _t->errorHandler((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->errorHandler((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< video*(*)>(_a[2]))); break;
        case 10: _t->parseUpdateInfo((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 11: _t->fetchVideoInfo((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: _t->clearCurrentVideo(); break;
        case 13: _t->enqueueDownload((*reinterpret_cast< video*(*)>(_a[1]))); break;
        case 14: _t->cancelAllDownloads(); break;
        case 15: _t->search((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 16: _t->search(); break;
        case 17: _t->clipboardChanged(); break;
        case 18: _t->activateProxySettings(); break;
        case 19: _t->startUpdateDownload(); break;
        case 20: _t->skipUpdate(); break;
        case 21: _t->updateDownloadFinished(); break;
        case 22: _t->updateDownloadProgress((*reinterpret_cast< qint64(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2]))); break;
        case 23: _t->updateReadyRead(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< video* >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< video* >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< video* >(); break;
            }
            break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< video* >(); break;
            }
            break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< video* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ClipGrab::*)(video * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ClipGrab::currentVideoStateChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (ClipGrab::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ClipGrab::downloadEnqueued)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (ClipGrab::*)(video * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ClipGrab::downloadFinished)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (ClipGrab::*)(video * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ClipGrab::searchFinished)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (ClipGrab::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ClipGrab::youtubeDlDownloadFinished)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (ClipGrab::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ClipGrab::compatibleUrlFoundInClipboard)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (ClipGrab::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ClipGrab::allDownloadsCanceled)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (ClipGrab::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ClipGrab::updateInfoProcessed)) {
                *result = 7;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ClipGrab::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_ClipGrab.data,
    qt_meta_data_ClipGrab,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ClipGrab::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ClipGrab::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ClipGrab.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int ClipGrab::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    }
    return _id;
}

// SIGNAL 0
void ClipGrab::currentVideoStateChanged(video * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ClipGrab::downloadEnqueued()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void ClipGrab::downloadFinished(video * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ClipGrab::searchFinished(video * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void ClipGrab::youtubeDlDownloadFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void ClipGrab::compatibleUrlFoundInClipboard(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void ClipGrab::allDownloadsCanceled()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void ClipGrab::updateInfoProcessed()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

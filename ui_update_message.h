/********************************************************************************
** Form generated from reading UI file 'update_message.ui'
**
** Created by: Qt User Interface Compiler version 5.15.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UPDATE_MESSAGE_H
#define UI_UPDATE_MESSAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include <web_engine_view.h>

QT_BEGIN_NAMESPACE

class Ui_UpdateMessage
{
public:
    QGridLayout *gridLayout;
    QLabel *labelDownloadProgress;
    QSpacerItem *verticalSpacer;
    QPushButton *buttonSkip;
    QPushButton *buttonLater;
    QProgressBar *progressBar;
    QPushButton *buttonConfirm;
    QWidget *widget;
    QGridLayout *gridLayout_2;
    QLabel *labelInfoText;
    QLabel *label;
    QWidget *widget_2;
    QFormLayout *formLayout;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *horizontalSpacer_2;
    CGWebEngineView *webEngineView;

    void setupUi(QDialog *UpdateMessage)
    {
        if (UpdateMessage->objectName().isEmpty())
            UpdateMessage->setObjectName(QString::fromUtf8("UpdateMessage"));
        UpdateMessage->resize(600, 450);
        UpdateMessage->setModal(true);
        gridLayout = new QGridLayout(UpdateMessage);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setHorizontalSpacing(20);
        gridLayout->setVerticalSpacing(10);
        gridLayout->setContentsMargins(20, 20, 20, 20);
        labelDownloadProgress = new QLabel(UpdateMessage);
        labelDownloadProgress->setObjectName(QString::fromUtf8("labelDownloadProgress"));

        gridLayout->addWidget(labelDownloadProgress, 5, 0, 1, 4);

        verticalSpacer = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer, 2, 1, 1, 1);

        buttonSkip = new QPushButton(UpdateMessage);
        buttonSkip->setObjectName(QString::fromUtf8("buttonSkip"));
        buttonSkip->setAutoDefault(false);

        gridLayout->addWidget(buttonSkip, 7, 0, 1, 1);

        buttonLater = new QPushButton(UpdateMessage);
        buttonLater->setObjectName(QString::fromUtf8("buttonLater"));
        buttonLater->setAutoDefault(false);

        gridLayout->addWidget(buttonLater, 7, 2, 1, 1);

        progressBar = new QProgressBar(UpdateMessage);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(24);

        gridLayout->addWidget(progressBar, 6, 0, 1, 4);

        buttonConfirm = new QPushButton(UpdateMessage);
        buttonConfirm->setObjectName(QString::fromUtf8("buttonConfirm"));

        gridLayout->addWidget(buttonConfirm, 7, 3, 1, 1);

        widget = new QWidget(UpdateMessage);
        widget->setObjectName(QString::fromUtf8("widget"));
        gridLayout_2 = new QGridLayout(widget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        labelInfoText = new QLabel(widget);
        labelInfoText->setObjectName(QString::fromUtf8("labelInfoText"));
        labelInfoText->setWordWrap(true);

        gridLayout_2->addWidget(labelInfoText, 1, 2, 1, 1);

        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("color:#00b2de;\n"
"font-size: 18px;"));

        gridLayout_2->addWidget(label, 0, 2, 1, 1);

        widget_2 = new QWidget(widget);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget_2->sizePolicy().hasHeightForWidth());
        widget_2->setSizePolicy(sizePolicy);
        widget_2->setMinimumSize(QSize(64, 64));
        formLayout = new QFormLayout(widget_2);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setHorizontalSpacing(0);
        formLayout->setVerticalSpacing(0);
        formLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(widget_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(64, 64));
        label_2->setMaximumSize(QSize(32, 32));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/img/icon.png")));
        label_2->setScaledContents(true);

        formLayout->setWidget(0, QFormLayout::LabelRole, label_2);


        gridLayout_2->addWidget(widget_2, 0, 0, 2, 1);

        horizontalSpacer = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 0, 1, 1, 1);


        gridLayout->addWidget(widget, 0, 0, 2, 4);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 7, 1, 1, 1);

        webEngineView = new CGWebEngineView(UpdateMessage);
        webEngineView->setObjectName(QString::fromUtf8("webEngineView"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(100);
        sizePolicy1.setHeightForWidth(webEngineView->sizePolicy().hasHeightForWidth());
        webEngineView->setSizePolicy(sizePolicy1);
        webEngineView->setMinimumSize(QSize(0, 150));
        webEngineView->setUrl(QUrl(QString::fromUtf8("about:blank")));

        gridLayout->addWidget(webEngineView, 4, 0, 1, 4);

        QWidget::setTabOrder(buttonConfirm, buttonLater);
        QWidget::setTabOrder(buttonLater, buttonSkip);

        retranslateUi(UpdateMessage);
        QObject::connect(buttonLater, SIGNAL(clicked()), UpdateMessage, SLOT(accept()));
        QObject::connect(buttonSkip, SIGNAL(clicked()), UpdateMessage, SLOT(reject()));

        buttonConfirm->setDefault(true);


        QMetaObject::connectSlotsByName(UpdateMessage);
    } // setupUi

    void retranslateUi(QDialog *UpdateMessage)
    {
        UpdateMessage->setWindowTitle(QCoreApplication::translate("UpdateMessage", "Update for ClipGrab", nullptr));
        labelDownloadProgress->setText(QCoreApplication::translate("UpdateMessage", "The update will begin in just a moment \342\200\246", nullptr));
        buttonSkip->setText(QCoreApplication::translate("UpdateMessage", "Skip this update", nullptr));
        buttonLater->setText(QCoreApplication::translate("UpdateMessage", "Remind me later", nullptr));
        buttonConfirm->setText(QCoreApplication::translate("UpdateMessage", "Download update", nullptr));
        labelInfoText->setText(QCoreApplication::translate("UpdateMessage", "ClipGrab %1 is now available (you are using %2). Would you like to install the update?", nullptr));
        label->setText(QCoreApplication::translate("UpdateMessage", "There is an update for your version of ClipGrab!", nullptr));
    } // retranslateUi

};

namespace Ui {
    class UpdateMessage: public Ui_UpdateMessage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UPDATE_MESSAGE_H
